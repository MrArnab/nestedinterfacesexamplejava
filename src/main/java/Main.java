/**
 * Created by Marco on 20/09/16.
 */
public class Main
{
    public static void main(String [] args)
    {
        TopLevelInterface obj = new InterfaceImplementation();
        obj.methodA();
        obj.methodB();
    }

}
