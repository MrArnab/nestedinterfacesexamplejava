/**
 * Created by Marco on 20/09/16.
 */
public interface TopLevelInterface
{
    void methodA();
    void methodB();

    interface NestedInterface
    {
        void methodC();
    }

    public NestedInterface innerObj = null;

}
