/**
 * Created by Marco on 20/09/16.
 */
public class InterfaceImplementation implements TopLevelInterface
{

    public void methodA() {
        System.out.println("MethodA invoked");
    }

    public void methodB() {
        System.out.println("MethodB invoked");
    }
}
